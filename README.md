# 🖼️ Basic Border Banner

> Generate basic border banners for the terminal

## Basic Usage

### Nested Banner Demonstration

_Basic Border Banner_ is capable of surrounding even previously generated banners with a border:

```py
In [1]: from basic_border_banner import basic_border_banner

In [2]: print(
   ...:     basic_border_banner(
   ...:         basic_border_banner(
   ...:             "Basic\nBorder\nBanner\nGenerator",
   ...:             " *.*"
   ...:         ),
   ...:         "##"
   ...:     )
   ...: )
#################################
##                             ##
##   *.* *.* *.* *.* *.* *.*   ##
##   *.*                 *.*   ##
##   *.*    Basic        *.*   ##
##   *.*    Border       *.*   ##
##   *.*    Banner       *.*   ##
##   *.*    Generator    *.*   ##
##   *.*                 *.*   ##
##   *.* *.* *.* *.* *.* *.*   ##
##                             ##
#################################
```

### CLI Command `basic-border-banner`

```sh
❯ basic-border-banner --help
Usage: basic-border-banner [OPTIONS] MESSAGE BORDER_MARK

Arguments:
  MESSAGE      [required]
  BORDER_MARK  [required]

Options:
  --space TEXT                    [default:  ]
  --newline-alias TEXT            [default: |]
  --mirror-marks / --no-mirror-marks
                                  [default: mirror-marks]
  --install-completion [bash|zsh|fish|powershell|pwsh]
                                  Install completion for the specified shell.
  --show-completion [bash|zsh|fish|powershell|pwsh]
                                  Show completion for the specified shell, to
                                  copy it or customize the installation.
  --help                          Show this message and exit.
```

## [License: `MIT`](LICENSE.md)
