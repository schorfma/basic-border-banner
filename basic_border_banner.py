import math
import sys
from contextlib import redirect_stdout
from io import StringIO
from typing import TextIO


def basic_border_banner(
    message: str,
    border_mark: str,
    space: str = " ",
    newline_alias: str | None = "|",
    mirror_marks: bool = True,
    output: TextIO | None = None,
) -> str | None:
    """Generate a basic border banner from a (multiline) message."""

    if newline_alias:
        message = message.replace(newline_alias, "\n")

    if mirror_marks:
        mirrored_border_mark = border_mark[::-1]
    else:
        mirrored_border_mark = border_mark

    border_padding = space * len(border_mark)
    maximum_message_line_length = max(
        len(message_line) for message_line in message.splitlines()
    )

    maximum_banner_line_length = maximum_message_line_length + 2 * (
        len(border_mark) + len(border_padding)
    )

    horizontal_border: str = border_mark * math.ceil(
        maximum_banner_line_length / len(border_mark)
    )
    horizontal_border = horizontal_border[:maximum_banner_line_length]

    horizontal_spacing = space * maximum_message_line_length

    if output is None:
        output = StringIO()

    with redirect_stdout(output):
        print(horizontal_border)
        print(border_mark, horizontal_spacing, mirrored_border_mark, sep=border_padding)

        for message_line in message.splitlines():
            message_line += space * (maximum_message_line_length - len(message_line))
            print(border_mark, message_line, mirrored_border_mark, sep=border_padding)

        print(border_mark, horizontal_spacing, mirrored_border_mark, sep=border_padding)
        print(horizontal_border)

    if isinstance(output, StringIO):
        return output.getvalue()


try:
    import typer
except ImportError:

    def basic_border_banner_cli() -> None:
        basic_border_banner(
            input("Message: "), input("Border Mark: "), output=sys.stdout
        )

else:
    basic_border_banner_cli = typer.Typer()

    @basic_border_banner_cli.command("basic-border-banner")
    def basic_border_banner_wrapper(
        message: str,
        border_mark: str,
        space: str = " ",
        newline_alias: str = "|",
        mirror_marks: bool = True,
    ) -> None:
        basic_border_banner(
            message, border_mark, space, newline_alias, output=sys.stdout
        )


if __name__ == "__main__":
    basic_border_banner_cli()
